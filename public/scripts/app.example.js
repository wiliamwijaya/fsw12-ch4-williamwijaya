class App {
  constructor() {
    // this.clearButton = document.getElementById("clear-btn");
    // this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.submitButton = document.getElementById("submit-btn");
  }

  async init() {
    await this.load();

    this.submitButton.addEventListener("click", () => {
      this.clear();
      this.filterCar();
    });
  }

  filterCar = () => {
    // source reference renal
    let driver = document.getElementById("tipeDriverInput").value;
    let jumlahPenumpang = document.getElementById("jumlahPenumpangInput").value;
    let date = document.getElementById("date").value;
    let timeValue = document.getElementById("time").value;
    let rent = date + "T" + timeValue;
    let formDate = Date.parse(rent);

    Car.list.forEach((car) => {
      let renting = Date.parse(car.availableAt);
      let time = car.availableAt;
      let Time = time.toLocaleTimeString();

      let tempdate = JSON.stringify(car.availableAt);
      let tempdate2 = tempdate.split("T");
      let tempdate3 = tempdate2[0].replace('"', "");

      if (
        jumlahPenumpang <= car.capacity &&
        driver == car.available &&
        Time >= timeValue &&
        tempdate3 >= date
      ) {
        if (renting >= formDate) {
          const node = document.createElement("div");
          node.className = "card";
          node.innerHTML = car.render();
          this.carContainerElement.appendChild(node);
        }
        if (driver == car.available) {
          const node = document.createElement("div");
          node.className = "card";
          node.innerHTML = car.render();
          this.carContainerElement.appendChild(node);
        }
      } else if (driver == "null" && jumlahPenumpang <= car.capacity) {
        if (renting >= formDate || (Time >= timeValue && tempdate3 >= date)) {
          const node = document.createElement("div");
          node.className = "card";
          node.innerHTML = car.render();
          this.carContainerElement.appendChild(node);
        }
      }
    });
  };

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.className = "card";
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
