class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
        <div class="card-body">
          <div>
            <img src="${this.image}"  width="100%">
          </div>
          <div id="car-spec">
            <div>
              <h4>${this.manufacture} ${this.model}</h4>
              <h2>Rp ${this.rentPerDay} / hari</h2>
              <p>${this.description}</p>
            </div>
            <div class="car-spec-icon">
              <p>
                <i class="fa-solid fa-user-group"></i>
                ${this.capacity} orang
              </p>
              <p>
                <i class="fa-solid fa-gear"></i>
                ${this.transmission}
              </p>
              <p>
                <i class="fa fa-calendar"></i>
                Tahun ${this.year}
              </p>
              <button class="btn btn-success car-btn">Pilih Mobil</button>
            </div>
          </div>
        </div>
    `;
  }
}
